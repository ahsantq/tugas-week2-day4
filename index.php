<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
// Bentuk fungsi
require ('animal.php');
$sheep = new Animal("shaun");

echo "Name : ",$sheep->name; // "shaun"
echo "<br>legs : ",$sheep->legs; // 4
echo "<br>cold blooded : ",$sheep->cold_blooded; // "no"

$kodok = new Frog("buduk");
echo "<br><br>Name : ",$kodok->name;
echo "<br>legs : ",$kodok->legs;
echo "<br> cold blooded : ",$kodok->cold_blooded;
echo "<br>Jump : ",$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "<br><br>Name : ",$sungokong->name;
echo "<br>legs : ",$sungokong->legs;
echo "<br>cold blooded : ",$sungokong->cold_blooded;
echo "<br>Yell : ",$sungokong->yell(); // "Auooo"
?>
</body>
</html>