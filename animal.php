
<?php

class Animal 
{
  public $name;
  public $legs = 4;
  public $cold_blooded = "no";
  public function __construct($name) 
  {
    $this->name= $name;
  }
}
class Frog extends Animal
{
  public $cold_blooded = "yes";
  public function jump(){
    echo "hop hop";
  }
  public function __construct($name) 
  {
    $this->name= $name;
  }
}
class Ape extends Animal
{
  Public $legs = 2;
  public function yell(){
    echo "Auooo";
  }
  public function __construct($name) 
  {
    $this->name= $name;
  }
}
?>